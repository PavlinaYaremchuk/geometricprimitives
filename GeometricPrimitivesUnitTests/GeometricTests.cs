using NUnit.Framework;
using Geometric_primitives;

namespace GeometricPrimitivesUnitTests
{
    public class GeometricTests
    {
        [Test]
        public void RectanglePerimeterArea()
        {
            var rectangle = new Rectangle(4, 6);

            Assert.AreEqual(24, rectangle.Area());
            Assert.AreEqual(20, rectangle.Perimeter());
        }
        [Test]
        public void SquarePerimeterArea()
        {
            var square = new Square(4);

            Assert.AreEqual(16, square.Area());
            Assert.AreEqual(16, square.Perimeter());
        }

        [Test]
        public void DiamondPerimeterArea()
        {
            var diamond = new Diamond(3, 5);

            Assert.AreEqual(15, diamond.Area());
            Assert.AreEqual(12, diamond.Perimeter());
        }

        [Test]
        public void CirclePerimeterArea()
        {
            var circle = new Circle(4);
            Assert.AreEqual(50.27, circle.Area());
            Assert.AreEqual(25.13, circle.Perimeter());
        }

        [Test]
        public void ParallelogramPerimeterArea()
        {
            var parallelogram = new Parallelogram(5,6,10);

            Assert.AreEqual(50, parallelogram.Area());
            Assert.AreEqual(22, parallelogram.Perimeter());
        }

        [Test]
        public void TriangelePerimeterArea()
        {
            var triangele = new Triangle (5, 6, 10, 7);

            Assert.AreEqual(17.5, triangele.Area());
            Assert.AreEqual(21, triangele.Perimeter());
        }

        [Test]
        public void TrapeziumPerimeterArea()
        {
            var trapezium = new Trapezium(5, 6, 10, 7, 8);

            Assert.AreEqual(55, trapezium.Area());
            Assert.AreEqual(26, trapezium.Perimeter());
        }

        [Test]
        public void SumsOfPerimeters_and_SumsOfAreas()
        {
            GeometricPrimitives[] shapes = { new Rectangle(4, 6), new Square(4), new Diamond(3, 5), new Circle(4), new Parallelogram(5, 6, 10), new Triangle(5, 6, 10, 7), new Trapezium(5, 6, 10, 7, 8) };
            double sumAreas = 0;
            double sumPerimeters = 0;
            foreach (var item in shapes)
            {
                sumAreas += item.Area();
                sumPerimeters += item.Perimeter();
            }
            
            Assert.AreEqual(227.77, sumAreas);
            Assert.AreEqual(142.13, sumPerimeters);
        }
    }

}