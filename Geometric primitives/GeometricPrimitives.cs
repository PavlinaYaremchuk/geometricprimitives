﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public abstract class GeometricPrimitives
    {
        public abstract double Area();
        public abstract double Perimeter();
    }
}
