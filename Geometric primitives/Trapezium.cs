﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Trapezium : GeometricPrimitives
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Height { get; set; }
        public double Side3 { get; set; }
        public double Side4 { get; set; }

        public Trapezium(double side1, double side2, double height, double side3, double side4)
        {
            Side1 = side1;
            Side2 = side2;
            Height = height;
            Side3 = side3;
            Side4 = side4;
        }

        public override double Area()
        {
            return (Side1 + Side2) / 2 * Height;
        }

        public override double Perimeter()
        {
            return Side1 + Side2 + Side3 + Side4;
        }
    }
}
