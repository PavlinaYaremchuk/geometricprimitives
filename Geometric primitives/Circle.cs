﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Circle : GeometricPrimitives
    {
        public double Radius { get; set; }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public override double Area()
        {
            return Math.Round(Radius * Radius * Math.PI, 2);
        }

        public override double Perimeter()
        {
            return Math.Round(Radius * 2 * Math.PI, 2);
        }
    }
}
