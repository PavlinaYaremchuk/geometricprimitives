﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Diamond : GeometricPrimitives
    {
        public double Side { get; set; }
        public double Height { get; set; }

        public Diamond(double side, double height)
        {
            Side = side;
            Height = height;
        }

        public override double Area()
        {
            return Side * Height;
        }

        public override double Perimeter()
        {
            return Side * 4;
        }
    }
}
