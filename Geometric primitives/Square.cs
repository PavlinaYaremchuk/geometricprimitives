﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Square : GeometricPrimitives
    {
        public double Side { get; set; }

        public Square(double side)
        {
            Side = side;
        }

        public override double Area()
        {
            return Side * Side;
        }

        public override double Perimeter()
        {
            return Side * 4;
        }
    }
}
