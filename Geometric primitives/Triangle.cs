﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Triangle : GeometricPrimitives
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Side3 { get; set; }
        public double HeightToSide1 { get; set; }

        public Triangle(double side1, double side2, double side3, double heightToSide1)
        {
            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
            HeightToSide1 = heightToSide1;
        }
        public override double Area()
        {
            return 0.5 * Side1 * HeightToSide1;
        }
        public override double Perimeter()
        {
            return Side1 + Side2 + Side3;
        }
    }
}
