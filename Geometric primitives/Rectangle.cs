﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Rectangle : GeometricPrimitives
    {
        public double Width { get; set; }
        public double Length { get; set; }

        public Rectangle(double width, double length)
        {
            Width = width;
            Length = length;
        }

        public override double Area()
        {
            return Width * Length;
        }

        public override double Perimeter()
        {
            return (Width + Length) * 2;
        }
    }
}
