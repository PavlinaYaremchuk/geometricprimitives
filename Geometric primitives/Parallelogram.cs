﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Geometric_primitives
{
    public class Parallelogram : GeometricPrimitives
    {
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Side1Height { get; set; }

        public Parallelogram(double side1, double side2, double side1Height)
        {
            Side1 = side1;
            Side1Height = side1Height;
            Side2 = side2;
        }

        public override double Area()
        {
            return Side1 * Side1Height;
        }

        public override double Perimeter()
        {
            return (Side1 + Side2) * 2;
        }
    }
}
